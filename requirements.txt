Django==3.2.13
django-debug-toolbar==3.2.4
django-crispy-forms==1.13.0
django-downloadview==2.3.0
django-js-reverse==0.9.1
Pillow==9.0.1
sorl-thumbnail==12.7.0
django-environ-2==2.3.0
